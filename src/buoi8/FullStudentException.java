package buoi8;

public class FullStudentException extends Exception {

    public FullStudentException() {}

    public FullStudentException(String message) {
        super(message);
    }
}
