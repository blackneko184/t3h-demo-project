package buoi8;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) throws Exception {
        Student student1 = new Student(1, "Long", LocalDate.of(1992,2,29));
        Student student2 = new Student(2, "Lam", LocalDate.of(1993,1,27));
        Student student3 = new Student(3, "Hoang", LocalDate.of(1994,6,21));
        Student student4 = new Student(4, "Quang", LocalDate.of(1991,3,17));
        Student student5 = new Student(5, "", LocalDate.of(1971,1,1));
        Student student6 = new Student(6, "Hieu", LocalDate.of(1970,1,1));

        List<Student> studentList = new ArrayList<Student>(5);
            studentList.add(student1);
            studentList.add(student2);
            studentList.add(student3);
            studentList.add(student4);
            studentList.add(student5);
//            studentList.add(student6);

        try {
            checkValueList(studentList);
        } catch (FullStudentException e) {
            System.out.println("Class is fulled");
        } catch (BirthYearException e) {
            System.out.println("Student is not exist");
        } catch (NameExistException e) {
            System.out.println("Name is not blank");
        }
        System.out.println("end");

    }

    static void checkValueList(List<Student> studentList) throws FullStudentException, NameExistException {
        if (studentList.size() <= 5) {
            for (int i = 0; i < studentList.size(); i++) {
                if (studentList.get(i).getBirthDay().isAfter(LocalDate.of(1970,1,1))) {
                    System.out.println((studentList.get(i).toString()));
                } else {
                    throw new BirthYearException();
                }
                if (studentList.get(i).getName().isEmpty()){
                    throw new NameExistException();
                }
            }
        } else {
            throw new FullStudentException();
        }
    }

}
